package com.example.plataformaupt.Api.Servicios;

import com.example.plataformaupt.ViewModels.RegistroUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.GET;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

}
